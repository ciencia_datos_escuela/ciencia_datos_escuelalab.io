# Ejemplos

## Conectividad en Argentina
* **Archivos:** [:material-comma-box-outline:](Conectividad_Internet.csv) [:material-language-python:](Conectividad_Analisis.ipynb)

## Shows de Netflix y los Oscars
* **Archivos:** [:material-comma-box-outline:](./estadisticas_netflix_oscars/netflix_titles.csv) [:material-file-excel-box:](./estadisticas_netflix_oscars/oscars.xlsx) [:material-language-python:](./estadisticas_netflix_oscars/Películas.ipynb)

## Salud Escolar
* **Archivos:** [:material-comma-box-outline:](./salud_escolar/EMSE_DatosAbiertos.csv) [:material-file-excel-box:](./salud_escolar/libro-de-codigos-emse-2018.xlsx) [:material-language-python:](./salud_escolar/EMSE_Analisis.ipynb)

## Nacimientos en Argentina
* **Archivos:** [:material-comma-box-outline:](./datos_nacimiento/Nacimientos_Arg_2005-2010.csv) [:material-language-python:](./datos_nacimiento/Demo_CDS_nacimientos.ipynb)

## Rampas geolocalizadas en el mapa de CABA
* **Archivos:** [:material-zip-disk:](./rampas_caba/comunas.zip) [:material-file-excel-box:](./rampas_caba/rampas-de-accesibilidad-relevamiento-2016.csv) [:material-language-python:](./rampas_caba/Mapa de rampas por estado en CABA año 2016.ipynb)

