# Ciencia De Datos En Escuelas

Proyecto de extensión de la Facultad de Informática - UNLP  
La Plata - Buenos Aires
## Descripción

En este repositorio se encuentran los recursos armados para docentes de nivel secundario orientado al análisis de datos para su utilización en diferentes áreas curriculares. El proyecto propone introducir el análisis y visualización de datos en propuestas pedagógicas que se puedan llevar adelante en el aula de la escuela secundaria.
Se utilizan herramientas informáticas como el entorno de ejecución Jupyter Notebook (o similar) que permite de forma interactiva el trabajo dinámico y participativo por parte de los estudiantes con el lenguaje de programación Python y herramientas adecuadas al análisis y visualización de datos. Las herramientas utilizadasson de uso libre y permiten el trabajo remoto y colaborativo entre estudiantes.

Este repositorio contiene materiales y guías, talleres de capacitación, videos etc. 

## Recursos

* Cuadernos en Jupyter de diferentes temáticas utilizando la librería Pandas para analizar los datos, Matplotlib y Plotly para graficar
* Videos introductorios de utilización de la herramienta Colab, alternativa online a Jupyter

## Videos

* [:material-video: Primer análisis!](https://www.youtube.com/watch?v=_M6AC-lqOYM)

## Instalación

Para instalar localmente Jupyter se puede realizar de varias formas:

* Utilizando la herramienta **pip** :

	> pip install jupyter

* Cargar el notebook y el archivo desde colab

## Uso
Puede descargar el repositorio y ejecutar el entorno Jupyter, todos los ejemplos se encuentran disponibles para su uso con el archivo de datos que utiliza en la misma carpeta

## Integrantes
Integramos este proyecto

* Claudia Banchoff
* Isabel Kimura
* Sofía Martin
* Juan Vicens, Arianda Aspitia
* Ailén Panigo
* Matías Batisti
* Agustín Genoves
* Ulises Geymonat

## Licencia
Todo el materal realizado es de licencia :Atribución-CompartirIgual CC BY-SA
Los archivos de datos fueron descargados de sitios diferentes, para su uso consulte cada uno.